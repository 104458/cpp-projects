#include <iostream>
#include <windows.h>
#include <unistd.h>
#include <limits>
#include <fstream>
#include <cstdlib>
using namespace std;

string strInp;
int intInp;
int id;

int loginPassword()
{
    ifstream loginp{ "password.data"};
    if(!loginp)
    {
        cout << " error, cant open file";
    }
    while(loginp)
    {
        cout << " Enter password : ";
        string data;
        string login_data;
        cin >> data;
        loginp.seekg(0);
        getline(loginp, login_data);
        if(data==login_data)
        {
            cout << "\n Logged in";
        }
        else
        {
            cout << " Password incorrect!";
        }
        sleep(1);
        return 0;
    }

}

int loginUser()
{
    ifstream login{ "username.data"};
    if(!login)
    {
        cout << " error, cant open file";
    }
    while(login)
    {
        cout << "\n Enter username : ";
        string data;
        string login_data;
        cin >> data;
        login.seekg(0);
        getline(login, login_data);
        if(data==login_data)
        {
            loginPassword();
        }
        else
        {
            cout << "\n Username not found!";
        }

        sleep(1);
        return 0;
    }

}

int generateId()
{
    ofstream idfile{ "identf.data"};
    if(!idfile)
    {
        cout << " error, cant open file";
    }
    while(idfile)
    {
        id = 1+(rand()%998);
        idfile << id;
        cout << "\n Your ID is(>" << id << ") ";
        for (int a = 1; a < 6; a++)
        {
            if(a==1)
            {
                cout << "\n continue in";
            }
            cout << "\n " << a;
            sleep(1);
        }
        sleep(1);
        return 0;
    }

}

int createPass()
{
    ofstream infile{ "password.data"};
    if(!infile)
    {
        cout << " error, cant open file";
    }
    while(infile)
    {
        cout << " Enter password: ";
        cin >> strInp;
        infile.seekp(0);
        infile << strInp;
        generateId();

        return 0;
    }

}

int createUser()
{
    ofstream outfile{ "username.data"};
    if(!outfile)
    {
        cout << " error, cant open file";
    }
    while(outfile)
    {
        cout << "\n Enter username: ";
        cin >> strInp;
        outfile.seekp(0);
        outfile << strInp;
        createPass();

        return 0;
    }

}

int selection()
{
    int select;
    cin >> select;
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(),'\n');

    switch(select)
    {
    case 1:
        system("CLS");
        cout << "\n LOGIN";
        loginUser();
        break;
    case 2:
        system("CLS");
        cout << "\n REGISTER";
        createUser();
        break;
    case 3:
        system("CLS");
        cout << " finding";
        break;
    case 4:
        system("CLS");
        cout << " modifying";
        break;
    case 5:
        system("CLS");
        cout << " deleting";
        break;
    case 6:
        cout << " exitt";
        exit ( 3);
        break;
    default:
        cout << " selection not found!";
        sleep(1);

    }
}



void cls(){
#if defined(_WIN32) //if windows
    system("cls");

#else
    system("clear");    //if other
#endif  //finish
}

